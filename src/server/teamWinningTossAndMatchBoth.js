function teamTossAndMatchWin(matchesJsonArray){
    const winningTeam = {};

    for(let index = 0; index < matchesJsonArray.length; index++){
        let entry = matchesJsonArray[index];
        let matchWinner = entry.winner;

        if(winningTeam[matchWinner] === undefined && entry.toss_winner === matchWinner){
            winningTeam[matchWinner] = 1;
        }
        else if(winningTeam[matchWinner] !== undefined && entry.toss_winner === matchWinner){
            winningTeam[matchWinner]++;
        }
    }

    // console.log(winningTeam);
    return winningTeam;
}

module.exports = teamTossAndMatchWin;

const matchesFilePath = "./src/data/matches.csv";
const csvMatches = require("csvtojson");
csvMatches()
.fromFile(matchesFilePath)
.then((jsonObj)=>{
    console.log(teamTossAndMatchWin(jsonObj));
})