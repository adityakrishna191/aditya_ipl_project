function playerOfMatchPerSeason(matchesJsonArray){
    const playerOfMatchObj = {};

    for(let index = 0; index < matchesJsonArray.length; index++){
        let entry = matchesJsonArray[index];
        let playerOfMatch = entry.player_of_match;
        let season = entry.season;

        if(playerOfMatchObj[season] === undefined){
            playerOfMatchObj[season] = {};
            playerOfMatchObj[season][playerOfMatch] = 1;
        }
        else{
            if(playerOfMatchObj[season][playerOfMatch] === undefined){
                playerOfMatchObj[season][playerOfMatch] = 1;
            }
            else{
                playerOfMatchObj[season][playerOfMatch]++;
            }            
        }        
    }
    // console.log(playerOfMatchObj);

    const seasonAndPlayerOfMatch = {};
    const seasonsArr = Object.keys(playerOfMatchObj);
    
    for(let year of seasonsArr){
        const playerOfMatch = playerOfMatchObj[year];
        const playerOfMatchSorted = Object.entries(playerOfMatch).sort((a,b)=>(b[1]-a[1]));
        // console.log(playerOfMatchSorted[0]);
        seasonAndPlayerOfMatch[year] = playerOfMatchSorted[0][0];
    }    
    // console.log(seasonAndPlayerOfMatch);
    return seasonAndPlayerOfMatch;
}

module.exports = playerOfMatchPerSeason;

const matchesFilePath = "./src/data/matches.csv";
const csvMatches = require("csvtojson");
csvMatches()
.fromFile(matchesFilePath)
.then((jsonObj)=>{
    console.log(playerOfMatchPerSeason(jsonObj));
})