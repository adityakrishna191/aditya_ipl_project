const csv = require("csvtojson");
const fs = require("fs");
const extraRunsPerTeam = require("./extraRunsPerTeamIn2016.js");
const matchesWonPerTeamPerYear = require("./matchesWonPerTeamPerYear.js");
const matchesPerYear = require("./matchesPerYear.js");
const bowlersEconomy = require("./top10EconomicalBowlersIn2015.js");


const matchesFilePath = "../data/matches.csv";
const deliveriesFilePath = "../data/deliveries.csv";

csv()
    .fromFile(matchesFilePath)
    .then((matches) => {
        csv()
            .fromFile(deliveriesFilePath)
            .then((deliveries) => {


                let matchesPerYearResult = matchesPerYear(matches);
                matchesPerYearResult = JSON.stringify(matchesPerYearResult);
                fs.writeFile("../public/output/matchesPerYear.json", matchesPerYearResult, (err) => {
                    if (err) {
                        console.log('error', err);
                    }
                    else {
                        console.log('DONE');
                    }
                })

                let matchesWonPerTeamPerYearResult = matchesWonPerTeamPerYear(matches);
                matchesWonPerTeamPerYearResult = JSON.stringify(matchesWonPerTeamPerYearResult);
                fs.writeFile("../public/output/matchesWonPerTeamPerYear.json", matchesWonPerTeamPerYearResult, (err) => {
                    if (err) {
                        console.log('error', err);
                    }
                    else {
                        console.log('DONE');
                    }
                })

                let extraRunsPerTeamIn2016Result = extraRunsPerTeam(matches, deliveries);
                extraRunsPerTeamIn2016Result = JSON.stringify(extraRunsPerTeamIn2016Result);
                fs.writeFile("../public/output/extraRunsPerTeamIn2016.json", extraRunsPerTeamIn2016Result, (err) => {
                    if (err) {
                        console.log('error', err);
                    }
                    else {
                        console.log('DONE');
                    }
                })

                let bowlersEconomyResult = bowlersEconomy(matches, deliveries);
                bowlersEconomyResult = JSON.stringify(bowlersEconomyResult);
                fs.writeFile("../public/output/top10EconomicalBowlersIn2015.json", bowlersEconomyResult, (err) => {
                    if (err) {
                        console.log('error', err);
                    }
                    else {
                        console.log('DONE');
                    }
                })

            })
    })