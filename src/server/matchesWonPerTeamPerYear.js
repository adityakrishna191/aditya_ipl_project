function matchesWonPerTeamPerYear(jsonArray) {
    const perYearWinningTeamObj = {};
    for(let index = 0; index < jsonArray.length; index++){
        let entry = jsonArray[index];
        let matchYear = entry["season"];
        let matchWinner = entry["winner"];
        if(matchWinner!==""){
            if(perYearWinningTeamObj[matchWinner]===undefined){
                perYearWinningTeamObj[matchWinner] = {};

                if(perYearWinningTeamObj[matchWinner][matchYear]===undefined){
                    perYearWinningTeamObj[matchWinner][matchYear] = 1;
                }        
            }
            else{
                if(perYearWinningTeamObj[matchWinner][matchYear]===undefined){
                    perYearWinningTeamObj[matchWinner][matchYear] = 1;
                }
                else{
                    perYearWinningTeamObj[matchWinner][matchYear]++;
            
                }
            }
        }        
    }
    return perYearWinningTeamObj;
}

module.exports = matchesWonPerTeamPerYear;