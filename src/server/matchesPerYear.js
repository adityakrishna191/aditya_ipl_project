function matchesPerYear(jsonArray) {
    const matchesPerYearObj = {}
    for (let index = 0; index < jsonArray.length; index++) {
        let entry = jsonArray[index];
        let yearOfMatch = entry["season"];
        if (matchesPerYearObj[yearOfMatch] === undefined) {
            matchesPerYearObj[yearOfMatch] = 1;
        } else {
            matchesPerYearObj[yearOfMatch]++;
        }
    }
    return matchesPerYearObj;
}

module.exports = matchesPerYear;
