function bestEconomyInSuperOver(deliveriesJsonArray){
    const runsAndDeliveryByBowler = {};

    
    for(let deliveriesIndex = 0; deliveriesIndex < deliveriesJsonArray.length; deliveriesIndex++){
        let entry = deliveriesJsonArray[deliveriesIndex];
        
        if(entry.is_super_over !== "0"){//check condition for super-over, rest of the code is from earlier bowlerEconomy function
            // console.log(entry);
            let bowlerName = entry.bowler;
            let runsConceded = Number(entry.total_runs);
            
            if(runsAndDeliveryByBowler[bowlerName] === undefined){
                runsAndDeliveryByBowler[bowlerName] = {};
                runsAndDeliveryByBowler[bowlerName]["runsConceded"] = runsConceded;
                runsAndDeliveryByBowler[bowlerName]["deliveriesBowled"] = 0
            }
            else{
                runsAndDeliveryByBowler[bowlerName]["runsConceded"] += runsConceded;
                runsAndDeliveryByBowler[bowlerName]["deliveriesBowled"] += 1;
            }
        }
    }
    // console.log(runsAndDeliveryByBowler);
    const bowlerEconomy = {};
    
    for(let bowler in runsAndDeliveryByBowler){
        // console.log(runsAndDeliveryByBowler[bowler].runsConceded, runsAndDeliveryByBowler[bowler].deliveriesBowled);
        let economy = runsAndDeliveryByBowler[bowler].runsConceded/ (runsAndDeliveryByBowler[bowler].deliveriesBowled/6);
        // console.log(economy);
        bowlerEconomy[bowler] = economy;
    }
    // console.log(bowlerEconomy);
    
    const bowlerEconomyArrSorted = Object.entries(bowlerEconomy).sort((a,b)=>a[1]-b[1]);
    // console.log(bowlerEconomyArrSorted);
    return bowlerEconomyArrSorted[0];
}

module.exports = bestEconomyInSuperOver;

const deliveriesFilePath = "./src/data/deliveries.csv";
const csvDeliveries = require("csvtojson");
csvDeliveries()
.fromFile(deliveriesFilePath)
.then((jsonObj)=>{
    console.log(bestEconomyInSuperOver(jsonObj));
})