// function seasonByMatchId(matchesJsonArray, matchId){
//     for(let index = 0; index < matchesJsonArray.length; index++){
//         const entry = matchesJsonArray[index];
//         if(entry.id === matchId){
//             return entry.season;
//         }
//     }
// }

// const matchesFilePath = "./src/data/matches.csv";
// const csvMatches = require("csvtojson");
// csvMatches()
// .fromFile(matchesFilePath)
// .then((jsonObj)=>{
//     console.log(seasonByMatchId(jsonObj)); //How to return the season in StrikeRateFn?
// })

function batsmanStrikeRateEachSeason(deliveriesJsonArray){
    const batsmanStrikeRateObj = {};
    
    for(let index = 0; index < deliveriesJsonArray.length; index++){ //deliveriesJsonArray.length
        const entry = deliveriesJsonArray[index];
        const batsman = entry.batsman;
        const runsScoredByBatsman = Number(entry.batsman_runs);
        const matchId = entry.match_id;
        //const season = seasonByMatchId(matchesJsonObj, matchId);

        if(batsmanStrikeRateObj[batsman] === undefined){
            batsmanStrikeRateObj[batsman] = {};
            batsmanStrikeRateObj[batsman]["runsScored"] = runsScoredByBatsman;
            batsmanStrikeRateObj[batsman]["ballsFaced"] = 1;
        }
        else{
            batsmanStrikeRateObj[batsman]["runsScored"] += runsScoredByBatsman;
            batsmanStrikeRateObj[batsman]["ballsFaced"]++;
        }
    }
    // console.log(batsmanStrikeRateObj);

    //Strike rate = (runsScored/ballsFaced)*100

    const strikeRateObj = {};
    
    for(let batsman in batsmanStrikeRateObj){
        let strikeRate = (batsmanStrikeRateObj[batsman].runsScored/ batsmanStrikeRateObj[batsman].ballsFaced)*100;
        // console.log(strikeRate);
        strikeRateObj[batsman] = strikeRate;
    }
    // console.log(strikeRateObj);
    return strikeRateObj;
}

module.exports = batsmanStrikeRateEachSeason;

const deliveriesFilePath = "./src/data/deliveries.csv";
const csvDeliveries = require("csvtojson");
csvDeliveries()
.fromFile(deliveriesFilePath)
.then((jsonObj)=>{
    console.log(batsmanStrikeRateEachSeason(jsonObj));
})