
function bowlersEconomy(matchesJsonArray, deliveriesJsonArray) {
    const runsAndDeliveryByBowler = {};

    for (let matchesIndex = 0; matchesIndex < matchesJsonArray.length; matchesIndex++) { //matchesJsonArray.length
        let matchesEntry = matchesJsonArray[matchesIndex];
        if (matchesEntry.season === "2015") {

            for (let deliveriesIndex = 0; deliveriesIndex < deliveriesJsonArray.length; deliveriesIndex++) {
                let entry = deliveriesJsonArray[deliveriesIndex];

                if(matchesEntry.id === entry.match_id) {
                    // console.log(entry);
                    let bowlerName = entry.bowler;
                    let runsConceded = Number(entry.total_runs);
                    // let oversBowled = 0;
                    if (runsAndDeliveryByBowler[bowlerName] === undefined) {
                        runsAndDeliveryByBowler[bowlerName] = {};
                        runsAndDeliveryByBowler[bowlerName]["runsConceded"] = runsConceded;
                        runsAndDeliveryByBowler[bowlerName]["deliveriesBowled"] = 0
                    }
                    else {
                        runsAndDeliveryByBowler[bowlerName]["runsConceded"] += runsConceded;
                        runsAndDeliveryByBowler[bowlerName]["deliveriesBowled"] += 1;
                    }
                }
            }
        }        
    }
    // console.log(runsAndDeliveryByBowler);
    const bowlerEconomy = {};

    for (let bowler in runsAndDeliveryByBowler) {
        // console.log(runsAndDeliveryByBowler[bowler].runsConceded, runsAndDeliveryByBowler[bowler].deliveriesBowled);
        let economy = runsAndDeliveryByBowler[bowler].runsConceded / (runsAndDeliveryByBowler[bowler].deliveriesBowled / 6);
        // console.log(economy);
        bowlerEconomy[bowler] = Number(economy.toFixed(2));
    }
    // console.log(bowlerEconomy);

    const bowlerEconomyArrSorted = Object.entries(bowlerEconomy).sort((a, b) => a[1] - b[1]);
    // console.log(bowlerEconomyArrSorted);

    const top10BowlerByEconomy = [];
    for (let index = 0; index < 10; index++) {
        top10BowlerByEconomy.push(bowlerEconomyArrSorted[index][0], bowlerEconomyArrSorted[index][1]);
    }
    // console.log(top10BowlerByEconomy);
    return top10BowlerByEconomy;
}

module.exports = bowlersEconomy;