function playerDismissedByAnother(deliveriesJsonArray){
    const playerDismissedObj = {};
    for(let index = 0; index < deliveriesJsonArray.length; index++){ //deliveriesJsonArray.length
        const entry = deliveriesJsonArray[index];
        const playerDismissed = entry.player_dismissed;
        const bowler = entry.bowler;

        if(playerDismissed !== ""){
            
            if(playerDismissedObj[playerDismissed] === undefined){
                playerDismissedObj[playerDismissed] = {};
    
                playerDismissedObj[playerDismissed][bowler] = 1
            }
            else if(playerDismissedObj[playerDismissed][bowler] === undefined){

                playerDismissedObj[playerDismissed][bowler] = 1;
            }
            else{
                playerDismissedObj[playerDismissed][bowler]++;
            }
        }        
    }
    // console.log(playerDismissedObj);

    const highestDismissalOfPlayerByAnotherPlayer = {};

    for(let player in playerDismissedObj){
        const bowlerListSorted = Object.entries(playerDismissedObj[player]).sort((a,b)=>(b[1]-a[1]));
        // console.log(bowlerListSorted[0]);
        highestDismissalOfPlayerByAnotherPlayer[player] = bowlerListSorted[0];
    }
    // console.log(highestDismissalOfPlayerByAnotherPlayer);
    // return highestDismissalOfPlayerByAnotherPlayer;
    
    // console.log(Object.entries(highestDismissalOfPlayerByAnotherPlayer)
    // .sort((a,b)=>b[1][1]-a[1][1])
    // [0])
    return Object.entries(highestDismissalOfPlayerByAnotherPlayer)
    .sort((a,b)=>b[1][1]-a[1][1])
    [0];
}

module.exports = playerDismissedByAnother;

const deliveriesFilePath = "./src/data/deliveries.csv";
const csvDeliveries = require("csvtojson");
csvDeliveries()
.fromFile(deliveriesFilePath)
.then((jsonObj)=>{
    console.log(playerDismissedByAnother(jsonObj));
})