
function extraRunsPerTeam(matchesJsonArray, deliveriesJsonArray) {
    const extraRunsObj = {};

    for (let matchesIndex = 0; matchesIndex < matchesJsonArray.length; matchesIndex++) { //matchesJsonArray.length
        let matchesEntry = matchesJsonArray[matchesIndex];
        if (matchesEntry.season === "2016") {
            
            for (let deliveriesIndex = 0; deliveriesIndex < deliveriesJsonArray.length; deliveriesIndex++) {
                let entry = deliveriesJsonArray[deliveriesIndex];
                
                if(matchesEntry.id === entry.match_id){
                    // console.log(entry);
                    let bowlingTeam = entry.bowling_team;
                    let extraRuns = Number(entry.extra_runs);

                    if (extraRunsObj[bowlingTeam] === undefined) {
                        extraRunsObj[bowlingTeam] = 0;
                        extraRunsObj[bowlingTeam] += extraRuns;
                    }
                    else {
                        extraRunsObj[bowlingTeam] += extraRuns;
                    }
                }
            }
        }
    }

    // console.log(extraRunsObj);
    return extraRunsObj;
}

module.exports = extraRunsPerTeam;